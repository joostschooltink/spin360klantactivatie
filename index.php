<?php

// Include config
require("config.php");

// Include classes
require("classes/Bootstrap.php");
require("classes/Controller.php");
require("classes/Logging.php");
require("classes/Model.php");
require("classes/Messages.php");

// Include controllers
require("controllers/homeController.php");

// Include models
require("models/homeModel.php");

$bootstrap = new Bootstrap($_GET);
$controller = $bootstrap->createController();
if ($controller)
{
    $controller->executeAction();
}
